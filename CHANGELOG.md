## [0.1.2](https://gitlab.com/soccrates-eu/charts/rp/compare/0.1.1...0.1.2) (2022-06-29)


### Bug Fixes

* adds imagePullPolicy with secret ([a9cae9b](https://gitlab.com/soccrates-eu/charts/rp/commit/a9cae9b50c2fbb98bd57c3b90e4c6f2b28be9d43))

## [0.1.1](https://gitlab.com/soccrates-eu/charts/rp/compare/0.1.0...0.1.1) (2022-06-29)


### Bug Fixes

* values ([c5b0b65](https://gitlab.com/soccrates-eu/charts/rp/commit/c5b0b65cbc6ee262c87552f8e0036420954308e6))
